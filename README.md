[![Netlify Status](https://api.netlify.com/api/v1/badges/5f377221-50cf-4b48-b610-d61c243df8de/deploy-status)](https://app.netlify.com/sites/pizzasyntax/deploys)
# Pizza lekérdező oldal - kezdő verzió - React-ban építve
Ez a pizza lekérdező oldal React-ban lett megírva és ASP.NET backend-ről (Linuxos) fetch-el le végpontokat. 
A CRUD teljes és ez az egész csak oktatási célra készült.
A frontend a nemethb.linkpc.net - ingyenes domain alá lett deployolva,
míg a backend egy - szintén ingyenes - sulla.ddns.net domain alá. Egyiknek sincs közvetlen A-rekordja, csak aldomain-t kaptam hozzá, illetve CNAME-et kellett beállítani a Netlify-hoz.

# Frontend
[Frontend](https://nemethb.linkpc.net)

# Végpontok listája (swagger):
[Swagger](https://pizza.kando-dev.eu/swagger/index.html)

# Pizza backend:
[Pizza végpont](https://pizza.kando-dev.eu/Pizza)<br>
[Pizza végpont ID-ra (pl.1)](https://pizza.kando-dev.eu/Pizza/1)

# Deploy: 
Netlify-on (link fent), hozzá kapcsoltam egy domain nevet, SSL-el. Sajnos a backend-nek még vannak gyerekbetegségei, így nem kezeli rendesen az SSL-t, ezért a legtöbb böngésző szerint nem biztonságos.

# Known issues / ismert problémák:
A végpont csak akkor mutat teljes funkcionalitást, ha sikerült korábban betölteni legalább az egyik pizza végpontot ([pizza végpont](https://pizza.kando-dev.eu/Pizza))
Ez a probléma a helytelen https-certificate telepítéséből adódott ASP.NET Linux környezetben (work in progress)
